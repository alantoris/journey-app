# Journey


## Development

```bash

Build:
docker-compose -f local.yml build

Up:
docker-compose -f local.yml up

Up with django in a secondary console
docker-compose -f local.yml up
docker-compose -f local.yml ps
docker rm -f app-viaje_django_1
docker-compose run --rm --service-ports django


Comandos administrativos
docker-compose run --rm django COMMAND
```
